Simple test app for test RESTful query


REQUIREMENTS

Python 2.7

Flask==0.12.1
Jinja2==2.9.6

INSTALLATION

git clone http://<user>@stash.opentact.org/scm/tp/callback-test.git

RUN

cd callback-test python ttest.py
nohup python ttest.py &

By default app listen port 8000
If you need you can change port in string ttest.py to another port.

	app.run(host='0.0.0.0', port=8000, debug=True)

For stop you need kill process by PID, like this command

	kill $(ps aux | grep 'ttest.py' | awk '{print $2}')

For authentification see USERNAME\PASSWORD variables in ttest.py

DATABASE

As database test app use a sqlite3 file.

schema contains two table:

	CREATE TABLE entries (
	  id integer primary key autoincrement,
	  key text not null UNIQUE,
	  response text not null
	);
	CREATE TABLE history (
	    id integer primary key autoincrement,
	    key text not null,
	    htime text not null,
	    method text not null,
	    request text not null,
	    response text not null
	);


UI
	After app start UI accessible by address http://<host>:port

	Main page view:

	Table with Key-Response pair. Against every key placed History and Edit links.

	You can update Response text through Edit form.

	For add new Key-Response pair you can use form "Add" on main page.

	History link - For view history all requests by Key.


CMD USAGE:

	For testing http query you can use curl:

		curl -i http://localhost:8000/test_takt/Play


		HTTP/1.0 200 OK
		Content-Type: application/xml; charset=utf-8
		Content-Length: 171
		Link: http://localhost
		Server: Werkzeug/0.12.1 Python/2.7.6
		Date: Wed, 10 May 2017 07:41:21 GMT

		<?xml version="1.0" encoding="UTF-8" ?>
		<Response>
		    <Speak>This is Plivo Cloud</Speak>
		    <Play>https://s3.amazonaws.com/plivocloud/Trumpet.mp3</Play>
		</Response>

