drop table if exists entries;
create table entries (
  id integer primary key autoincrement,
  key text not null UNIQUE,
  response text not null
);
drop table if exists history;
create table history (
    id integer primary key autoincrement,
    key text not null,
    htime text not null,
    method text not null,
    request text not null,
    response text not null
);


