# all the imports
import sqlite3
from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash, Response, Request, Blueprint
from contextlib import closing
import datetime
#from itertools import izip

# configuration
DATABASE = './testt.db'
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'adm'
PASSWORD = 'LrtMNs1'
#APPLICATION_ROOT='/callback'

#create simple test

bp = Blueprint('burritos', __name__,
                        template_folder='templates')


class CherrokeeFix(object):

    def __init__(self, app, script_name):
        self.app = app
        self.script_name = script_name

    def __call__(self, environ, start_response):
        path = environ.get('SCRIPT_NAME', '') + environ.get('PATH_INFO', '')
        environ['SCRIPT_NAME'] = self.script_name
        environ['PATH_INFO'] = path[len(self.script_name):]
        assert path[:len(self.script_name)] == self.script_name
        return self.app(environ, start_response)



app = Flask(__name__)
app.register_blueprint(bp, url_prefix='/callback')
app.config.from_object(__name__)

app.config.from_envvar('ITJAM_SETTINGS', silent=True)
#app.config["APPLICATION_ROOT"] = "/callback"

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()


@app.route('/')
def show_entries():
    cur = g.db.execute('select key, response from entries order by id desc')
    entries = [dict(key=row[0], response=row[1]) for row in cur.fetchall()]
    return render_template('show_entries.html', entries=entries)


app.wsgi_app = CherrokeeFix(app.wsgi_app, '/callback')


#decorator  for key adding
#add processing exception for double key
@app.route('/add', methods=['POST'])
def add_entry():
    if not session.get('logged_in'):
        abort(401)
    g.db.execute('insert into entries (key, response) values (?, ?)',
                 [request.form['title'], request.form['text']])
    g.db.commit()
    flash('New key was sucessfully posted')
    return redirect(url_for('show_entries'))
#edit route
@app.route('/edit/<key>', methods=['GET','POST'])
def edit_entry(key):
    if not session.get('logged_in'):
        abort(401)
    if request.method == 'POST':
        g.db.execute('update entries set response=? \
                      where key=?',(request.form['text'],key))
        g.db.commit()
        flash('Entry was successfully edited  ')
        return redirect(url_for('show_entries'))
    elif request.method != 'POST':
        cur = g.db.execute("select key, response, id from entries where key = "+ "'" + key + "'")
        entries = [{"key" : row[0], "text" : row[1] } for row in cur.fetchall()]
        return render_template('edit_entry.html', entries=entries)


#view history by key
@app.route('/history/<key>', methods=['GET','POST'])
@app.route('/history', methods=['GET','POST'])
def get_history(key):
    if not session.get('logged_in'):
        abort(401)
    if request.method == 'POST':
#        g.db.execute('update entries set response=? \
#                      where key=?',(request.form['text'],key))
#        g.db.commit()
#        flash('Entry was successfully edited  ')
        return redirect(url_for('show_entries'))
    elif request.method != 'POST':
        cur = g.db.execute("select key, htime, method, request, response from history where key = " + "'" + key + "'")
        entries = [dict(key=row[0], htime=row[1], method=row[2],  request=row[3],
        response=row[4] ) for row in cur.fetchall()]
        return render_template('show_history.html', entries=entries)

#RESTful
@app.route('/test_takt/<key>', methods=['GET','POST'])
def rest_key(key):
    cur = g.db.execute("select key, response, id from entries where key = "+ "'" + key + "'")
    entries = [{"key" : row[0], "text" : row[1] } for row in cur.fetchall()]

    js = entries[0]['text'] + "\n"
    resp = Response(js, status=200, mimetype='application/xml')
    resp.headers['Link'] = 'http://localhost'

    g.db.execute('insert into history (key, htime, method, request, response) values (?, ?, ?, ?, ?)',
            (key,datetime.datetime.now().strftime("%D:%H:%M"),request.method,request.url,"Content-type: "
                + resp.headers.get_all('Content-type')[0]
                + "\n" + "Status code: " + str(resp.status_code)
                + "\n" + js + "\n"))
    g.db.commit()

    return resp



@app.route('/login', methods=['GET','POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))

@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9090, debug=True)
